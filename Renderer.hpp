//
// Created by goksu on 2/25/20.
//
#include "Scene.hpp"
#include <mutex>
#include <functional>

#pragma once
struct hit_payload
{
    float tNear;
    uint32_t index;
    Vector2f uv;
    Object* hit_obj;
};

struct Rect
{
    int edge[4];
};

class Renderer
{
public:
    std::mutex mt_subrender_rect_pool;
    std::mutex mt_write_file;
    std::mutex mt_complete;
    std::vector<Rect> subrender_rect_pool;
    std::vector<Vector3f> framebuffer;
    float scale;
    float imageAspectRatio;
    Vector3f eye_pos;
    int spp;
    int rect_count;
    int thread_count;
    int subrender_rect_size;
    void Render(const Scene& scene);

private:
    void Subrender(const Scene& scene);
};
